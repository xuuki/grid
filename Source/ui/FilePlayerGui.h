/*
  ==============================================================================

    FilePlayerGui.h
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYERGUI
#define H_FILEPLAYERGUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/FilePlayer.h"

/**
 Gui for the FilePlayer class
 */
class FilePlayerGui :   public Component,
                        public FilenameComponentListener,
                        public Button::Listener,
                        public Slider::Listener

{
public:
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    FilePlayerGui();
    
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    FilePlayerGui(FilePlayer* fp);
    
    /**
     Destructor 
     */
    ~FilePlayerGui();
    
    void setFilePlayer (FilePlayer* fp);
    
    //Component
    void resized() override;
    
    //Button Listener
    void buttonClicked (Button* button)override ;
    
    //Slider Listener
    void sliderValueChanged (Slider* slider) override;
    
    //FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged) override;
    
private:
    TextButton playButton;
    TextButton playPosBut[16];
    TextButton pitchBut[16];
    
    Slider pitchSlid;
    
    ScopedPointer<FilenameComponent> fileChooser;
    // exersise says this is a pointer?? looks like a reference.
    FilePlayer* filePlayer;
};

#endif  // H_FILEPLAYERGUI
