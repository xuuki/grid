/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui() : filePlayer (nullptr)

{

    pitchSlid.addListener (this);
    pitchSlid.setRange (0, 5.0);
    addAndMakeVisible (&pitchSlid);
    
    
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    for (int i = 0; i < 16; i++)
    {
        playPosBut[i].addListener(this);
        addAndMakeVisible(&playPosBut[i]);
    }
    
    for (int i = 0; i < 16; i++)
    {
        pitchBut[i].addListener(this);
        addAndMakeVisible(&pitchBut[i]);
    }
    
    
}

FilePlayerGui::~FilePlayerGui()
{
    
}

void FilePlayerGui::setFilePlayer (FilePlayer* fp)
{
    filePlayer = fp;
}

//Component
void FilePlayerGui::resized()
{
    const int height = 20;
    
    playButton.setBounds (0, 0,  30,  height);
    fileChooser->setBounds (30, 0,  70, height);
    pitchSlid.setBounds (45, 0, 80 , height);
   

    for (int i = 0; i < 16; i++)
    {
      playPosBut[i].setBounds (((getWidth() / 16) - 5) * i , 20, 25 , height + 10);
    }
    
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
  
    if (button == &playButton)
    {
        filePlayer->setPlaying (!filePlayer->isPlaying());
        return;
    }
    
    for (int i = 0; i < 16; i++)
    {
        if (button == &playPosBut[i])
        {
            
            DBG (  " :" <<(float)i * 0.0625 << " play pos here \n");
            
            filePlayer->setPosition( (float)i * 0.0625);
            return;
        }
    }
    
}

//Slider Listener
void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    if(slider == &pitchSlid)
    {
         pitchSlid.setValue (pitchSlid.getValue(), dontSendNotification);
         filePlayer->setPlaybackRate(pitchSlid.getValue());
    }
    return;
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer->loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}
