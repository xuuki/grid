/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    setSize (500, 400);
    for (int i = 0; i < Audio::NumFilePlayers; i++)
    {
        addAndMakeVisible (&filePlayerGuis[i]);
        filePlayerGuis[i].setFilePlayer (audio.getFilePlayer(i));
    }
    
    mastGain.addListener (this);
    mastGain.setRange (0, 1);
    mastGain.setSliderStyle (Slider::LinearVertical);
    addAndMakeVisible (&mastGain);
    mastGain.setSkewFactorFromMidPoint (0.05);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    const int height = 50;
    for (int i = 0; i < Audio::NumFilePlayers; i++)
        filePlayerGuis[i].setBounds (0, i * height, getWidth(), height);
    
    mastGain.setBounds (420, 300, 80 , height + height);
   
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &mastGain)
    {
//        mastGain.setValue (mastGain.getValue(), dontSendNotification);
        audio.setGain(mastGain.getValue());
    }
    return;
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 400);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

