 /*
  ==============================================================================

    FilePlayer.cpp
    Created: 22 Jan 2013 2:49:14pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayer.h"

FilePlayer::FilePlayer() : thread("FilePlayThread")
{
//    thread.startThread();
//    currentAudioFileSource = nullptr;
//    resamplingAudioSource = new ResamplingAudioSource(&audioTransportSource, false);
    
    
    
    /** set required number of channels and buffer size
     */
    audioSampleBuffer.setSize (1, bufferSize);
    /** clear each element, to ensure it contains zeros only
     */
    audioSampleBuffer.clear();
    
    formatManager.registerBasicFormats();
    
    FileChooser chooser ("Please select the file you want to load...",
                         File::getSpecialLocation (File::userHomeDirectory),
                         formatManager.getWildcardForAllFormats());
    
    if (chooser.browseForFileToOpen())
    {
        File file (chooser.getResult());
        AudioFormatReader* reader = formatManager.createReaderFor (file);
        if (reader != 0)
        {
            audioSampleBuffer.setSize (reader->numChannels, reader->lengthInSamples);
            reader->read (&audioSampleBuffer, 0, reader->lengthInSamples, 0, true, false);
            delete reader;
        }
    }
    
}

/**
 Destructor
 */
FilePlayer::~FilePlayer()
{
//    delete resamplingAudioSource;
//    audioTransportSource.setSource (0);//unload the current file
//    if (currentAudioFileSource != nullptr)
//        deleteAndZero (currentAudioFileSource);//delete the current file
//    thread.stopThread(100);
}

/**
 Starts or stops playback of the looper
 */
//void FilePlayer::setPlaying (const bool newState)
//{
//    if(newState == true)
//    {
//        audioTransportSource.setPosition(0.0);
//        audioTransportSource.start();
//    }
//    else
//    {
//        audioTransportSource.stop();
//    }
//}

/**
 Gets the current playback state of the looper
 */
//bool FilePlayer::isPlaying () const
//{
//    return audioTransportSource.isPlaying();
//}

void FilePlayer::setPlayState (const bool newState)
{
    playState = newState;
}

bool FilePlayer::getPlayState () const
{
    return playState.get();
}

float FilePlayer::processSample (float input)
{
    float output = 0.f; // output variable
    if (playState.get() == true) //if play is enabled...
    {
        //assign output the current pos
        output = *audioSampleBuffer.getWritePointer(0, bufferPosition);
        
        //record
        //   if (recordState.get() == true) //not sure how to load the buffer
        //   &audioSampleBuffer = input;
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    }
    return output;
}

/**
 Loads the specified file into the transport source
 */
//void FilePlayer::loadFile(const File& newFile)
//{
//    /** unload the previous file source and delete it..
//     */
//    setPlaying(false);
//  //  audioTransportSource.setSource (0);
//    deleteAndZero (currentAudioFileSource);
//
//    /** create a new file source from the file..
//    */
//    /** get a format manager and set it up with the basic types (wav, ogg and aiff).
//     */
//    AudioFormatManager formatManager;
//    formatManager.registerBasicFormats();
//
//    AudioFormatReader* reader = formatManager.createReaderFor (newFile);
//
//    if (reader != 0)
//    {
//        //currentFile = audioFile;
//        currentAudioFileSource = new AudioFormatReaderSource (reader, true);
//
//        /** ..and plug it into our transport source
//         */
//        audioTransportSource.setSource (currentAudioFileSource,
//
//                                   32768, /** tells it to buffer this many samples ahead */
//                                   &thread,
//                                   reader->sampleRate);
//    }
//}

/** recieve values from slider */
//void FilePlayer::setPosition(float newPosition)
//{
//    audioTransportSource.setPosition(newPosition * audioTransportSource.getLengthInSeconds());
//     DBG (" : " <<  audioTransportSource.getLengthInSeconds() << " \n");
//}

void FilePlayer::setPlaybackRate(float newRate)
{
    resamplingAudioSource->setResamplingRatio(newRate);
}


//AudioSource
void FilePlayer::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    resamplingAudioSource->prepareToPlay (samplesPerBlockExpected, sampleRate);
}

void FilePlayer::releaseResources()
{
    resamplingAudioSource->releaseResources();
}

void FilePlayer::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    resamplingAudioSource->getNextAudioBlock (bufferToFill);
}
