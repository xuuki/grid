/*
  ==============================================================================

    FilePlayer.h
    Created: 22 Jan 2013 2:49:14pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYER
#define H_FILEPLAYER

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Simple FilePlayer class - streams audio from a file.
 */
class FilePlayer : public AudioSource,
                   public AudioSampleBuffer
{
public:
    /**
     Constructor
     */
    FilePlayer();
    
    /**
     Destructor
     */
    ~FilePlayer();
    
    /**
     Starts or stops playback of the looper
     */
//    void setPlaying (const bool newState);
    
    /** Starts or stops playback of the looper */
    void setPlayState (bool newState);
    
    /** Gets the current playback state of the looper */
    bool getPlayState() const;
    
    
    /**
     Gets the current playback state of the looper
     */
//    bool isPlaying () const;
//
    /**
     Loads the specified file into the transport source
     */
//    void loadFile (const File& newFile);
    
    /** to receive value from position slider
     */
    void setPosition (float newPosition);
    
    /** get values from pitch slider
    */
    void setPlaybackRate(float newRate);
    
    /** Processes the audio sample by sample. */
    float processSample (float input);
    
    /** AudioSource
     */
//    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
//    void releaseResources() override;
//    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    
    float* getWritePointer (int channelNumber, int sampleOffset);
    
private:
    
    //Shared data
    Atomic<int> recordState;
    Atomic<int> playState;
    
 //   AudioFormatReaderSource* currentAudioFileSource;    //reads audio from the file
 //   AudioTransportSource audioTransportSource;	// this controls the playback of a positionable audio stream, handling the
                                         // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                 //thread for the transport source
    ResamplingAudioSource* resamplingAudioSource;
  
    //Audio data
    static const int bufferSize = 88200; //constant
    unsigned int bufferPosition; //counter
    AudioSampleBuffer audioSampleBuffer; //array to store audio
    AudioFormatManager formatManager;
    
};

#endif  // H_FILEPLAYER
