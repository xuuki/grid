/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    enum  Constants
    {
        NumFilePlayers = 2
    };
    
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    FilePlayer* getFilePlayer (int index) { return &(filePlayers[index]); }
    
    Looper& getLooper() { return looper; }
   
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    
    void setGain(float value){ gain = value; }
private:
    AudioDeviceManager audioDeviceManager;
    MixerAudioSource mixerAudioSource;
    AudioSourcePlayer audioSourcePlayer;
    FilePlayer filePlayers[NumFilePlayers];
    Looper looper;
    float gain;
     JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Audio)
};

#endif  // AUDIO_H_INCLUDED
