/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"


Audio::Audio()
{
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
   
    for (int i = 0; i < Audio::NumFilePlayers; i++)
    {
        mixerAudioSource.addInputSource(&filePlayers[i], false);
    }
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource(&mixerAudioSource);
    
    //audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioSourcePlayer.setSource (nullptr);
    mixerAudioSource.removeAllInputs();
   // audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}


void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
                           
{
    /** get the audio from our file player - player puts samples in the output buffer
     */
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
//    float inSampL;
//    float inSampR;
    
//    float gain = mastGain.getValue();
    
    while(numSamples--)
    {
//        inSampL = *outL;
//        inSampR = *outL;
        
        float output = 0.f;
        
        output = looper.processSample (*inL);
        
        *outL = output * gain;
        *outR = output * gain;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

// prepare audioSourcePlayer for playback
void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    //we get the device for the audio information ?
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}
